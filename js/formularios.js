console.log('Script iniciado...!');

let list = document.getElementsByClassName('my-input');


function validaForm() {
    let list = document.getElementsByClassName('required');
    cleanAlerts();

    for (const data of list) {
        if(data.value == '') {
            let alert = document.createElement('p');
            alert.classList = 'alert';
            alert.innerHTML = 'El campo ' + data.getAttribute('name') + ' es obligatorio';
            data.parentElement.appendChild(alert);
        }
    }
}

function cleanAlerts() {
    let alerts = document.getElementsByClassName('alert');

    for (const item of alerts) {
        item.remove();
    }
}


document.getElementById('submit').addEventListener('click', ()=>{
    validaForm();
});


console.log('...Script finalizado!');